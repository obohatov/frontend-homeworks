1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

var - это способ объявления переменных, существовавший до внедрения нынешнего стандарта JavaScript (ECMAScript 6).
В новом стандарте функцию var взяли на себя let и const. 

let очень похоже на var, основное отличие — ограниченная область видимости переменных, объявляемых с его помощью.
Переменная, объявленная через var, видна везде, а переменная, объявленная через let, - только в рамках блока, в котором объявлена.
Кроме того, объявленная через var переменная может существовать в коде до ее объявления, в случае let - переменная используется в коде только после ее объявления.

const очень похоже на let, но у нее есть одно важное различие: const используется только для объявления констант.

2. Почему объявлять переменную через var считается плохим тоном?

var фактически создает глобальную переменную, использование которой (особенно в большом коде) потенциально чревато большим количеством ошибок. 
Тем не менее, мы должны учитывать использование var, если наш код взамодействует с чем-то, написанным на JS более 5 лет назад 