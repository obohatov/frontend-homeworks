const userName = prompt("What is your name?");
const userAge = +prompt("What is your age?");
const messageBan = `You are not allowed to visit this website`;
const messageWelcome = `Welcome, ${userName}`;
const messageCont = `Are you sure you want to continue?`;

if (userAge < 18) {
    alert(messageBan);
} else if (userAge <= 22) {
    let choice = confirm(messageCont);
    if (choice === true) {
        alert(messageWelcome);
    } else {
        alert(messageBan);
    }
} else {
    alert(messageWelcome);
}