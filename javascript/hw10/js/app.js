let showIcons = document.querySelectorAll('.icon-password');
showIcons.forEach(showIcon => {
    showIcon.addEventListener('click', evt => {
        evt.target.classList.toggle('fa-eye');
        evt.target.classList.toggle('fa-eye-slash');
        evt.target.classList.contains('fa-eye-slash') ? evt.target.previousElementSibling.type = 'text' : evt.target.previousElementSibling.type = 'password';
    })
});

let firstField = document.getElementById('first-field');
let secondField = document.getElementById('second-field');
let submitButton = document.querySelector('.btn');
let form = document.querySelector('form')
let warningMessage = document.createElement("span");
warningMessage.innerText = "Нужно ввести одинаковые значения";

submitButton.addEventListener('click', ev => {
    if (firstField.value === secondField.value && firstField.value !== "") {
        ev.preventDefault();
        alert('You are welcome');
        form.removeChild( warningMessage );
    } else {
        ev.preventDefault();
        form.appendChild( warningMessage );
        warningMessage.style.cssText = 'color:red';
    };
});
