let imgArr = document.querySelectorAll('.image-to-show');

imgArr.forEach((elem) => elem.style.display ='none');

let btnCont = document.createElement('button');
let bod = document.querySelector("body");
bod.prepend(btnCont);
btnCont.innerText = "Возобновить показ";

let btnStop = document.createElement('button');
bod.prepend(btnStop);
btnStop.innerText = "Прекратить";

let iCounter = 0;

let btnCheck = true;

let slideShow = () => {
    imgArr[iCounter].style.display = 'block';
    let slideCont = setTimeout(hideSlide, 10000);
    btnStop.addEventListener('click', () => {
        clearTimeout(slideCont);
        btnCheck = false;});
    btnCont.addEventListener('click', () => {
        if(btnCheck === false){
            slideCont = setTimeout(hideSlide, 10000);
            btnCheck = true;}});
};

let hideSlide = () => {
    imgArr[iCounter].style.display = 'none';
    iCounter += 1;
    if(iCounter === imgArr.length){
        iCounter = 0;
    }
    slideShow();
};

slideShow();