let btnCreate = document.createElement('button');
let bod = document.querySelector("body");
bod.prepend(btnCreate);
btnCreate.innerText = "Сменить тему";

function setTheme () {
    let style = document.createElement('link');
    style.rel = 'stylesheet';
    style.href = `./css/style${localStorage.getItem('active')}.css`;
    document.head.append(style);
}

if (!localStorage.getItem('active'))  localStorage.setItem('active', '0');

btnCreate.addEventListener('click', () => {
    localStorage.getItem('active') === '0' ? localStorage.setItem('active', '1') : localStorage.setItem('active', '0');
    setTheme();
    document.head.querySelector('link').remove();
});

setTheme();
