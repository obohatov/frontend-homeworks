$(document).ready(function(){
    $('.navbar-inner a').click(function(e){
        e.preventDefault();
        const anchor = $(document).find(`a[name$=${$(this).attr('href').slice(1)}]`)[0];
        $('html').animate({
            scrollTop: $(anchor).offset().top
        }, 1000);
    });

    $(".hot-news").append("<button class='hideSection'>Скрыть</button>");

    $(".hideSection").css({"background-color":"red", "display":"block", "margin":"0 auto 10px"});

    $(".global").append("<button class='slideTop'>Наверх</button>");

    $(".slideTop").css({"background-color":"greenyellow", "opacity":"0", "position":"fixed", "bottom": "80px", "right":"10px"});

    $('.hideSection').click(function(){
        $(this).prev().slideToggle('fast');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $(this).text('Показать')
        } else {
            $(this).text('Скрыть')

            const elSection = $(this).prev()[0];
            setTimeout( function() {
                $('html').scrollTop(elSection.offsetTop);
            }, 100);
        }
    });

    $(document).scroll(function() {
        if ($('html').scrollTop() > 530) {
            $(".slideTop").css({"opacity":"1"});
        } else {
            $(".slideTop").css({"opacity":"0"});
        }
    });

    $('.slideTop').click(function() {
        $('html').animate({
            scrollTop: 0
        }, 1000);
    });
});