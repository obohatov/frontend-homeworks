const num = +prompt("Please, give your number");
const message = "Sorry, no numbers";

if (num < 5 && num > -5) {
    alert(message);
} else if (num >= 5) {
	for (let i = 5; i <= num; i++) {
		if (i % 5 === 0) {
			alert(i);
		}
	}
} else {
   	for (let i = -5; i >= num; i--) {
		if (i % 5 === 0) {
			alert(i);
		}
	}
}