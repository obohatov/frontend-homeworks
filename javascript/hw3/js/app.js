const num1 = +prompt("Please, give your first number");
const num2 = +prompt("Please, give your second number");
const oper = prompt("What kind of math operation do you prefer? You have four options: +, -, *, /");

function calculator(a, b, sign) {
    switch (sign) {
        case "+":
            return a + b;
        case "-":
            return a - b;
        case "*":
            return a * b;
        case "/":
            return a / b;
    }
}

let result = calculator(num1, num2, oper);
alert( result );