function createNewUser () {
    let newUser = {
        firstName: prompt("What is your name?"),
        lastName: prompt("What is your surname?"),
        birthday: prompt("What is your birthday?", "dd.mm.yyyy"),

        getAge: function getAge() {
            let date = this.birthday.substr(0, 2),
                month = this.birthday.substr(3, 2) - 1,
                year = this.birthday.substr(6, 4),
                bDate = new Date(year, month, date),
                now = new Date(),
                period = now.getTime() - bDate.getTime();
            return Math.floor(period / (1000 * 60 * 60 * 24 * 365));
        },

        getPassword() {
            return this.firstName.substr(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(6, 4);
        },
    };
    
    return newUser;
}

let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());