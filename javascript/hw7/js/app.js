const words = ['1', '2', '3', 'sea', 'user', 23];
let ul = document.createElement('ul');
document.body.before(ul);

function toList(obj) {
    obj = obj.map(item => `<li>${item}</li>`);
    obj.reverse();
    for (item in obj) {
        let li = document.createElement('li');
        li.innerHTML = obj[item];
        ul.prepend(li);
    };
}

toList(words);