document.addEventListener("DOMContentLoaded", function() {
    let inputCreate = document.createElement('input');
    document.querySelector("body").appendChild(inputCreate);
    document.querySelector('input').placeholder = `Price`;
    let spanCorrect = document.createElement('span');
    let warningMessage = document.createElement('p');

    inputCreate.addEventListener("focus", (evt) => {
        evt.target.cssText = "border: 3px solid green";
    });
    inputCreate.addEventListener("blur", (evt) => {
        evt.target.cssText = "";
        price = inputCreate.value;

        if (price < 0) {
            inputCreate.style.cssText = "border: 3px solid red";
            warningMessage.innerText = "Please enter correct price";
            document.querySelector("body").appendChild(warningMessage);
            spanCorrect.remove();
            return;
        } else {
            warningMessage.remove();
        }

        if (inputCreate.value !== "") {
            spanCorrect.innerText = `Текущая цена: $` + price;
            document.body.insertBefore(spanCorrect, inputCreate);
            spanCorrect.style.cssText = "display: block";
        } else {
            spanCorrect.remove();
        }

        let spanButton = document.createElement('button');
        spanButton.innerText = `x`;
        spanCorrect.append(spanButton);

        inputCreate.style.cssText = "background-color: green";

        spanButton.addEventListener("click", () => {
            spanButton.remove();
            spanCorrect.remove();
            inputCreate.value = "0";
        })
    });
});