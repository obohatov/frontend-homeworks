document.querySelector('.tabs').addEventListener('click', switchTab);

function switchTab(event) {
    document.querySelector('.tabs-title.active').classList.remove('active');
    event.target.classList.add('active');
    const anchor = event.target.textContent;
    document.querySelector('.tabs-content li.active').classList.remove('active');
    document.querySelector(`[data-type="${anchor}"]`).classList.add('active')
}