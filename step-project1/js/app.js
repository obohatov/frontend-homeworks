$(document).ready(function(){

    $('.client-feedback-detail-list').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
    });

    $('.client-feedback-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.client-feedback-detail-list',
        arrows: true,
        focusOnSelect: true,
        centerMode: true,
        centerPadding: 50,
        prevArrow: '<i class="fas fa-chevron-left slider-prev-arrow"></i>',
        nextArrow: '<i class="fas fa-chevron-right slider-next-arrow"></i>',
    });

    $('.services-title').click((e) => {
        $('.services-title.active').removeClass('active');
        $(e.target).addClass('active');
        const anchor = $(e.target).text()
        $('.services-wrapper').removeClass("active");
        $(`.services-wrapper[data-service="${anchor}"]`).addClass("active");
    });

    $('.work-gallery-item').hover(
        function () {
            const $workGalleryItem = $(this);
            const workType = $workGalleryItem.data('workType');

            $workGalleryItem.append(`
                <div class="work-gallery-item-desc">
                    <p class="work-gallery-item-btns">
                        <button class="work-gallery-desc-btn btn-link"><i class="fa fa-link" aria-hidden="true"></i></button>
                        <button class="work-gallery-desc-btn btn-search"><i class="fa fa-square" aria-hidden="true"></i></button>
                    </p>
                    <span class="work-gallery-desc-title">creative design</span>
                    <span class="work-gallery-desc-type">${workType}</span>
                </div>
            `);
            $('.work-gallery-item-desc').hide().slideDown('fast')

        }, function () {
            $('.work-gallery-item-desc')
                .slideUp('fast', function () {
                    const $workGalleryItemDesc = $(this);
                    $workGalleryItemDesc.remove()
                })
        });

    $('.tab-work-filter-title').click(function() {
        const $tabWorkFilterTitle = $(this);
        $('.tab-work-filter-title.active').removeClass('active');
        $tabWorkFilterTitle.addClass('active');
        renderWorkGalery();
    });

    $('.sec-works-btn').click(function() {
        const $btn = $(this);
        renderSpinner($btn);

        setTimeout(() => {
            $('.spinner').remove();
            if ($('.load-more-second.hide').length > 0) {
                $('.load-more-second.hide').removeClass('hide');
            } else {
                $('.load-more-third.hide').removeClass('hide');
                $btn.hide();
            }
            renderWorkGalery();
        }, 2000)
    });

    function renderWorkGalery () {
        const $tabWorkFilterTitle = $('.tab-work-filter-title.active');
        const workType = $tabWorkFilterTitle.data('workType');

        if (workType !== 'All') {
            $('.work-gallery-item').hide();
            $(`.work-gallery-item[data-work-type="${workType}"]:not(.work-gallery-item.hide)`).show();
            $('.sec-works-btn').hide();
        } else {
            $('.work-gallery-item:not(.work-gallery-item.hide)').show();
            if ($('.load-more-third.hide').length > 0) {
                $('.sec-works-btn').show();
            }
        }
    }

    function renderSpinner ($el) {
        const elCoord = $el.offset();
        const elWidth = $el.outerWidth();

        $el
            .after('<i class="fas fa-spinner fa-spin"></i>')
            .next()
            .addClass('spinner')
            .offset({top: elCoord.top - 55, left: elCoord.left + elWidth/2 - 25});
    }
});